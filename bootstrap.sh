#!/usr/bin/env bash

directory=~/dev/projects/buttonfiles

$directory/.directories
$directory/.dotfiles
$directory/.templates
$directory/.macos
$directory/.brew_install
$directory/.brew
$directory/.cask
$directory/.preferences

# Finished
echo "$(basename "$0") complete."
