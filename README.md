# buttonfiles

A collection of scripts to bootstrap the setup and configuration of macOS.

## Acknowledgments

* [Craig Hurley](https://github.com/craighurley)
* [Mathias Bynens](https://github.com/mathiasbynens)